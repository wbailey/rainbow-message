Rainbow message

-- About --

cli program that makes a neat little pattern with a message using random colors and ascii emojis

-- Building --

run '$ make rainbow' to build the program

-- Usage --

./rainbow any message you like

will output 'any message you like'

-- Plans --

add getopts_long and some args, ignore strings that start with a '-' use a callback function to vary the patterns and output
