#include <stdio.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>

int rand_int_in_range (int max, int min);
void print_args (int argc, char *argv[], struct winsize w);
void clear (void);
char *ascii_emojis[241];

int main (int argc, char *argv[]) {
	srand (time(NULL));
	
	struct winsize w;
	ioctl (STDOUT_FILENO, TIOCGWINSZ, &w);
	clear ();

	while (1) {
		print_args (argc, argv, w);
	}

	return 0;
}

int rand_int_in_range (int min, int max) {
	int r = rand ();

	return r % (max + 1 - min) + min;
}

void print_args (int argc, char *argv[], struct winsize w) {		
	int r = rand_int_in_range (0, 255);
	int g = rand_int_in_range (0, 255);
	int b = rand_int_in_range (0, 255);	
	int i = 0;
	static int tabs = 0;
	static char *emoji;
	
	if (tabs == 0) {
		int rand_emoji_index = rand_int_in_range (0, sizeof(ascii_emojis) / sizeof(ascii_emojis[0]));
		emoji = ascii_emojis[rand_emoji_index];
	}

	for (i = 0; i < tabs; i++) {
		printf("\t");
	}
	
	tabs++;

	if (tabs > w.ws_col / 8) {
		tabs = 0;
	}
	
	printf ("\033[38;2;%d;%d;%dm%s ", r, g, b, emoji);
	for (i = 1; i < argc; i++) {
		printf("%s ", argv[i]);
	}

	printf ("%s\033[0m", emoji);
	printf ("\n");
	// fflush (stdout);

	sleep (2);
}

void clear (void) {
	printf ("\033[H");
	printf ("\033[J");
}

char *ascii_emojis[] = {	
	// innocent-face
	"ʘ‿ʘ",
	// reddit-disapproval-face
	"ಠ_ಠ",
	// table-flip
	"(╯°□°）╯︵ ┻━┻",
	// put-the-table-back
	"┬─┬﻿ ノ( ゜-゜ノ)",
	// tidy-up
	"┬─┬⃰͡ (ᵔᵕᵔ͜ )",
	// double-Flip
	"┻━┻ ︵ヽ(`Д´)ﾉ︵﻿ ┻━┻",
	// fisticuffs
	"ლ(｀ー´ლ)",
	// cute-bear
	"ʕ•ᴥ•ʔ",
	// squinting-bear
	"ʕᵔᴥᵔʔ",
	// GTFO-Bear
	"ʕ •`ᴥ•´ʔ",
	// cute-face-with-big-eyes
	"(｡◕‿◕｡)",
	// surprised
	"（　ﾟДﾟ）",
	// shrug-face
	"¯\\_(ツ)_/¯",
	// meh
	"¯\(°_o)/¯",
	// feel-perky
	"(`･ω･´)",
	// angry-face
	"(╬ ಠ益ಠ)",
	// at-what-cost
	"ლ(ಠ益ಠლ)",
	// excited
	"☜(⌒▽⌒)☞",
	// running
	"ε=ε=ε=┌(;*´Д`)ﾉ",
	// happy-face
	"ヽ(´▽`)/",
	// basking-in-glory
	"ヽ(´ー｀)ノ",
	// kitty-emote
	"ᵒᴥᵒ#",
	// fido
	"V•ᴥ•V",
	// meow
	"ฅ^•ﻌ•^ฅ",
	// cheers
	"（ ^_^）o自自o（^_^ ）",
	// devious-smile
	"ಠ‿ಠ",
	// 4chan-emoticon
	"( ͡° ͜ʖ ͡°)",
	// crying-face
	"ಥ_ಥ",
	// breat skdown
	"ಥ﹏ಥ",
	// disagree
	"٩◔̯◔۶",
	// flexing
	"ᕙ(⇀‸↼‶)ᕗ",
	// do-you-even-lift-bro?
	"ᕦ(ò_óˇ)ᕤ",
	// kirby
	"⊂(◉‿◉)つ",
	// tripping-out
	"q(❂‿❂)p",
	// discombobulated
	"⊙﹏⊙",
	// sad-and-confused
	"¯\\_(⊙︿⊙)_/¯",
	// japanese-lion-face
	"°‿‿°",
	// confused
	"¿ⓧ_ⓧﮌ",
	// confused-scratch
	"(⊙.☉)7",
	// worried
	"(´･_･`)",
	// dear-god-why
	"щ（ﾟДﾟщ）",
	// staring
	"٩(๏_๏)۶",
	// pretty-eyes
	"ఠ_ఠ",
	// strut
	"ᕕ( ᐛ )ᕗ",
	// zoned
	"(⊙_◎)",
	// crazy
	"ミ●﹏☉ミ",
	// trolling
	"༼∵༽ ༼⍨༽ ༼⍢༽ ༼⍤༽",
	// angry-troll
	"ヽ༼ ಠ益ಠ ༽ﾉ",
	// fuck-it
	"t(-_-t)",
	// sad-face
	"(ಥ⌣ಥ)",
	// hugger
	"(づ￣ ³￣)づ",
	// stranger-danger
	"(づ｡◕‿‿◕｡)づ",
	// flip-friend
	"(ノಠ ∩ಠ)ノ彡( \\o°o)\\",
	// cry-face
	"｡ﾟ( ﾟஇ‸இﾟ)ﾟ｡",
	// cry-troll
	"༼ ༎ຶ ෴ ༎ຶ༽",
	// tgif
	"“ヽ(´▽｀)ノ”",
	// dancing
	"┌(ㆆ㉨ㆆ)ʃ",
	// sleepy
	"눈_눈",
	// angry-birds
	"( ఠൠఠ )ﾉ",
	// no-support
	"乁( ◔ ౪◔)「      ┑(￣Д ￣)┍",
	// shy
	"(๑•́ ₃ •̀๑)",
	// fly-away
	"⁽⁽ଘ( ˊᵕˋ )ଓ⁾⁾",
	// careless
	"◔_◔",
	// love
	"♥‿♥",
	// touchy-feely
	"ԅ(≖‿≖ԅ)",
	// kissing
	"( ˘ ³˘)♥",
	// shark-face
	"( ˇ෴ˇ )",
	// emo-dance
	"ヾ(-_- )ゞ",
	// dance
	"♪♪ ヽ(ˇ∀ˇ )ゞ",
	// opera
	"ヾ(´〇`)ﾉ♪♪♪",
	// winnie-the-pooh
	"ʕ •́؈•̀)",
	// boxing
	"ლ(•́•́ლ)",
	// fight
	"(ง'̀-'́)ง",
	// listening-to-headphones
	"◖ᵔᴥᵔ◗ ♪ ♫",
	// robot
	"{•̃_•̃}",
	// seal
	"(ᵔᴥᵔ)",
	// questionable
	"(Ծ‸ Ծ)",
	// winning
	"(•̀ᴗ•́)و ̑̑",
	// zombie
	"[¬º-°]¬",
	// pointing
	"(☞ﾟヮﾟ)☞",
	// whistling
	"(っ•́｡•́)♪♬",
	// injured
	"(҂◡_◡)",
	// creeper
	"ƪ(ړײ)‎ƪ​​",
	// eye-roll
	"⥀.⥀",
	// flying
	"ح˚௰˚づ",
	// things-that-cant-be-unseen
	"♨_♨",
	// looking-down
	"(._.)",
	// im-a-hugger
	"(⊃｡•́‿•̀｡)⊃",
	// wizard
	"(∩｀-´)⊃━☆ﾟ.*･｡ﾟ",
	// yum
	"(っ˘ڡ˘ς)",
	// judging
	"( ఠ ͟ʖ ఠ)",
	// tired
	"( ͡ಠ ʖ̯ ͡ಠ)",
	// dislike
	"( ಠ ʖ̯ ಠ)",
	// hitchhiking
	"(งツ)ว",
	// satisfied
	"(◠﹏◠)",
	// sad-and-crying
	"(ᵟຶ︵ ᵟຶ)",
	// stunna-shades
	"(っ▀¯▀)つ",
	// chicken
	"ʚ(•｀",
	// barf
	"(´ж｀ς)",
	// fuck-off
	"(° ͜ʖ͡°)╭∩╮",
	// smiley-toast
	"ʕʘ̅͜ʘ̅ʔ",
	// exorcism
	"ح(•̀ж•́)ง †",
	// love
	"-`ღ´-",
	// taking-a-dump
	"(⩾﹏⩽)",
	// dab
	"ヽ( •_)ᕗ",
	// wave-dance
	"~(^-^)~",
	// happy-hug
	"\\(ᵔᵕᵔ)/",
	// resting-my-eyes
	"ᴖ̮ ̮ᴖ",
	// peepers
	"ಠಠ",
	// judgemental
	"{ಠʖಠ}",
	// sleepy flower girl
	"(◡ ‿ ◡ ✿)",
	// smiling breasts
	"（^人^）",
	// Nyan Cat
	"~=[,,_,,]:3",
	// koala bear
	"ʕ •ᴥ•ʔ",	
	// eff you
	"┌∩┐(◣_◢)┌∩┐",
	// sloth
	"(⊙ω⊙)",
	// middle finger up
	"t(-.-t)",
	// ooh sweetness
	"( ˘ ³˘)♥",
	// orly face
	"(• ε •)",
	// shrug
	"¯\\_(ツ)_/¯",
	// I dunno LOL
	"¯\\(º_o)/¯",
	// aggravated table flipping
	"‎(ﾉಥ益ಥ）ﾉ﻿ ┻━┻",
	// flipping Facebook
	"(╯°□°)╯︵ ʞooqǝɔɐɟ",
	// Gangnam Style
	"ヾ(⌐■_■)ノ♪",
	// patience young grasshopper
	"┬─┬﻿ ノ( ゜-゜ノ)",
	// It's Adventure Time
	"| (• ◡•)| (❍ᴥ❍ʋ)",
	// stupid ass memory leak
	"(╥﹏╥)",
	// alley-way snooper
	"┬┴┬┴┤(･_├┬┴┬┴",
	// Bear flipping table
	"ʕノ•ᴥ•ʔノ ︵ ┻━┻",
	// glomp
	"(づ￣ ³￣)づ",
	// drugged up happy
	"◉‿◉",
	// pudgy table flipping
	"(ノ ゜Д゜)ノ ︵ ┻━┻",
	// Hercules flipping tables
	"(/ .□.)\\ ︵╰(゜Д゜)╯︵ /(.□. \\)",
	// bro fist
	"(ó ì_í)=óò=(ì_í ò)",
	// flipping dude over
	"(╯°Д°）╯︵ /(.□ . \\)",
	// Super Saiyan Mode
	"ᕙ(⇀‸↼‶)ᕗ",
	// successful marriage proposal
	"(   ° ᴗ°)~ð  (/❛o❛\\)",
	// happy cat
	"(=^ェ^=)",
	// STOP! In the name of love
	"ノ(ジ)ー'",
	// marshmallow raising the roof
	"(づ｡◕‿‿◕｡)づ",
	// table flipped me
	"┬─┬﻿ ︵ /(.□. \\）",
	// emotional table flipping
	"(╯'□')╯︵ ┻━┻",
	// kawaii shrug
	"╮ (. ❛ ᴗ ❛.) ╭",
	// magical table flipping
	"(/¯◡ ‿ ◡)/¯ ~ ┻━┻",
	// baby seal
	"ᶘ ᵒᴥᵒᶅ",
	// I put back tables
	"(ノ^_^)ノ┻━┻ ┬─┬ ノ( ^_^ノ)",
	// zoidberg
	"(V) (°,,,,°) (V)",
	// dericious
	"(っ˘ڡ˘ς)",
	// table flipping battle
	"(╯°□°)╯︵ ┻━┻ ︵ ╯(°□° ╯)",
	// herp derp face
	"（。々°）",
	// happy hands up
	"ლ(╹◡╹ლ)",
	// love at first sight
	"♥‿♥",
	// loony face
	"(◐‿◑)﻿",
	// AWP
	// "︻デ═一",
	// dance party
	"♪┏(・o･)┛♪┗ ( ･o･) ┓♪",
	// PSP
	"(+[__]∙:∙)",
	// Pedobear is Watching
	"┬┴┬┴┤ʕ•ᴥ├┬┴┬┴",
	// Lorena Bobbit
	"( ＾◡＾)っ✂╰⋃╯",
	// Squid
	"&lt;コ:彡",
	// sparkles
	"☆.。.:*・°☆.。.:*・°☆.。.:*・°☆.。.:*・°☆",
	// one more thing
	// "(°ロ°)☝",
	// Would you like some cake?
	"( ・∀・)っ旦",
	// huzzah love
	"\\(-ㅂ-)/ ♥ ♥ ♥",
	// you da man
	"(σ・・)σ",
	// it's a celebration
	"＼(＾O＾)／",
	// Magic The Gathering Freakout
	"(ﾉಥДಥ)ﾉ︵┻━┻･/",
	// just sayin'
	"¬_¬",
	// stunna shades
	"(⌐■_■)",
	// zombie approaching
	"[¬º-°]¬",
	// dimpled smiling aura
	"｡◕ ‿ ◕｡",
	// face palm
	"(－‸ლ)",
	// Our prayers are with Boston
	// "(っ- ‸ -)っ ♥ Boston",
	// thinking hard
	// "♒((⇀‸↼))♒",
	// robot flipping tables
	// "┗[© ♒ ©]┛ ︵ ┻━┻",
	// you crazy
	"(☞ﾟ∀ﾟ)☞",
	// shuriken
	// "（ -.-）ノ-=≡≡卍",
	// yo-yo  pro
	"（*＾＾）/~~~~◎",
	// whale face
	"人◕ ‿‿ ◕人",
	// 1-percenter look of disapproval
	"ಠ_ರೃ",
	// Jedi flipping tables
	"(._.) ~ ︵ ┻━┻",
	// nyan kitty
	"[^._.^]ﾉ彡",
	// laughing arms up
	"(ノ・∀・)ノ",
	// high five for love
	"し(*・∀・)／♡＼(・∀・*) ///",
	// wide-eyed happy hands up
	"ლ(o◡oლ)﻿﻿",
	// blushing hearts
	"(｡´ ‿｀♡)",
	// wizard
	"(∩｀-´)⊃━☆ﾟ.*･｡ﾟ",
	// ping pong
	"ヽ(^o^)ρ┳┻┳°σ(^o^)/",
	// screamo lead singer
	"(ノ`Д ́)ノ",
	// squinty happy arms up
	"(ﾉ^_^)ﾉ",
	// boombox
	"|̲̅̅●̲̅̅|̲̅̅=̲̅̅|̲̅̅●̲̅̅|",
	// gimme a hug
	"(ღ˘⌣˘ღ)",
	// flower girl
	"(✿◕‿◕✿)",
	// love slug
	"(°◡°♡).:｡",
	// Iron Man
	"─=≡Σ(([ ⊐•̀⌂•́]⊐",
	// that creepy creeper
	"( ° ͜ʖ °)",
	// steaming mad
	"٩(̾●̮̮̃̾•̃̾)۶",
	// robot party
	"└[∵┌]└[ ∵ ]┘[┐∵]┘",
	// Tickets to the Gun Show
	"ᕙ( ͡° ͜ʖ ͡°)ᕗ",
	// Honey Boo Boo Child
	"(*☌ᴗ☌)｡",
	// blank critter stare
	"(・ω・)",
	// Last Twinkie in the World
	"ｏ(￣ρ￣)ｏ",
	// why hello
	"(｡◕‿◕｡)",
	// O Hai
	"(●°u°●)​ 」",
	// New iPhone Face
	"☆*✲ﾟ*｡(((´♡‿♡`+)))｡*ﾟ✲*☆",
	// angry old man
	"(ÒДÓױ)",
	// magic squirrel
	"ʕ⁎̯͡⁎ʔ༄",
	// in your face
	"(Ó_#)ò=(°□°ò)",
	// nitey nite
	"(- o - ) zzZ ☽",
	// The Macarena
	"(~^.^)~",
	// happy happy
	"ʘ‿ʘ",
	// together forever, I guess
	"(｡-_-｡ )人( ｡-_-｡)",
	// they see me rolling, they hating
	"(._.) ( l: ) ( .-. ) ( :l ) (._.)",
	// hello neighbor
	"|ʘ‿ʘ)╯",
	// the heckler
	"(ლ `Д ́)ლ",
	// yo wtf face
	"(⊙＿⊙')",
	// walrus
	"(:3 っ)っ",
	// headphones
	"d-_-b",
	// rock out
	"\\m/...(&gt;.&lt;)…\\m/",
	// happy birthday song
	"♪ (｡´＿●`)ﾉ┌iiii┐ヾ(´○＿`*) ♪",
	// Vampire Slayer
	"\\( •_•)_†",
	// eyebrow raised
	"٩(͡๏̯͡๏)۶",
	// fish
	"ϵ( 'Θ' )϶",
	// zen ninja
	"(-.-(-.(-(-.(-.-).-)-).-)-.-)",
	// Error 37
	"Diablo 3﻿ ︵ /(.□. \\）",
	// piggy
	"( ́・ω・`)",
	// Beyoncé at Super Bowl Halftime Show
	"♪~♪ ヽ໒(⌒o⌒)७ﾉ ♪~♪",
	// forced to harlem shake
	"┌( ಠ_ಠ)┘",
	// Domo-kun
	"\\|°▿▿▿▿°|/",
	// itadakimasu
	"ヾ(＠⌒ー⌒＠)ノ",
	// Mountain of Shame
	"༼;´༎ຶ ۝ ༎ຶ༽",
	// once upon a time, nobody gave a fuck
	"┐(￣ー￣)┌",
	// Would you like some pie?
	"( ・∀・)っ♨",
	// shrug alternate
	"¯\\_(シ)_/¯",
	// disapproving shrug
	"¯\\_ಠ_ಠ_/¯",
	// duck lips alternate
	"(・ε・)",
	// pew pew
	"(　-_･)σ - - - - - - - - ･",
	// headphones with mic
	"dL-_-b",
	// kirby
	"&lt;(-'.'-)&gt;",
	// bowling cat
	"◎ヽ(^･ω･^=)~",
	// fleeting love
	"(´∀｀)♡",
	// pigs need love too
	"(｡･ω･｡)ﾉ♡",
	// who's over there...
	"(  ⚆ _ ⚆ )",
	// lovely cheeks
	"♡＾▽＾♡",
	// nothing to see here
	"(̿▀̿ ̿Ĺ̯̿̿▀̿ ̿)",
	// Nintendo controller
	"[+..••]",
	// flipping Panda Laser
	"‎(╯°□°)╯︵ ɐpuɐd",
	// who farted?
	"ε=ε=ε=ε=ε=ε=┌(;￣◇￣)┘"
};
